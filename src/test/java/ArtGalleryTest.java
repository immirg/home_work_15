import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.time.Duration;


public class ArtGalleryTest {

    private WebDriver driver;
    private WebDriverWait wait;
    //private JavascriptExecutor js;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        //js = (JavascriptExecutor) driver;
        JavascriptExecutor js = (JavascriptExecutor) driver;
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        if (driver != null){
            driver.quit();
        }
    }

    @Test(priority = 1)
    public void openingPaintingElegantGrace(){
        driver.get("https://artgallery.in.ua/");
        WebElement searchPaintingElegantGrace = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id=\"div_hikashop_category_information_module_144_170\"]"))));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", searchPaintingElegantGrace);
        //js.executeScript("arguments[0].click();", searchPaintingElegantGrace);
        //searchPaintingElegantGrace.click();
        //javascriptExecutor.executeScript("arguments[0],value='Java';", searchPaintingElegantGrace);
    }

    @Test(priority = 2)
    public void checkingBasketAvailability(){
        wait.until(ExpectedConditions.textToBe(By.xpath("//*[@id=\"hikashop_cart_145\"]"), "Корзина пуста"));
    }

    @Test(priority = 3)
    public void checkingTitleThePainting(){
        wait.until(ExpectedConditions.textToBe(By.xpath("//*[@id=\"hikashop_product_name_main\"]"), "Элегантная грация"));
    }

    @Test(priority = 4)
    public void addingItemToCart(){
        WebElement addingPaintingToBasket = driver.findElement(By.xpath("//*[@id=\"hikashop_product_quantity_main\"]/a"));
        addingPaintingToBasket.click();
    }

    @Test(priority = 5)
    public void checkingThePaintingInTheCart(){
        WebElement openingBasketWithPainting = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"hikashop_cart_145\"]/a/span[contains(text(),'Итого:')]")));
        openingBasketWithPainting.click();
        WebElement checkingBasket = driver.findElement(By.xpath("//*[@id=\"hikashop_cart_dropdown_145\"]//*[contains(text(),'Всего')]"));
    }
}
